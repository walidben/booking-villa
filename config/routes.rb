Rails.application.routes.draw do
  root to: 'villas#index'
  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  resources :villas do 
    collection do
      get 'search'
    end
  end

  resources :reservations


  devise_for :users

end
