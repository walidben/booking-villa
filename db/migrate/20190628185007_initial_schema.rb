class InitialSchema < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.date :start_date
      t.date :end_date
      t.integer :nbAdults
      t.integer :nbChilds
      t.text :description
      t.integer :price
    end

    create_table :villas do |t|
      t.text :img_url
      t.string :country
      t.string :city
      t.string :address
      t.integer :price 
    end
  
  add_reference :reservations, :user, index: true
  add_reference :reservations, :villa, index: true
  end

end