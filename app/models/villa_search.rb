class VillaSearch
    attr_reader :date_start, :date_end

    def initialize(params)
        params ||= {}

        @date_start = params[:date_start]
        @date_end = params[:date_end]
    end

    def scope
        Villa.where.not(id: Reservation.select('villa_id').where('date(end_date) >= ? AND date(start_date) <= ?', date_start, date_end))
    end

end