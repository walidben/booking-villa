class VillasController < ApplicationController

    def index
        @search = VillaSearch.new(params[:search])
        @villas = @search.scope
    end

    def show
        @villa = Villa.find(params[:id])
    end
    
    def new
        @villa = Villa.new
    end
    
    
end
    