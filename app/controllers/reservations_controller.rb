require 'date'

class ReservationsController < ApplicationController
    before_action :authenticate_user!
    
    def index
    end

    def isDiscountable(start_date, end_date)
        if (start_date.to_date.wday >= 1 and start_date.to_date.wday < 5) and end_date.to_date.wday < 5
            true
        else
            false
        end
    end
        
    def show
        @reservation = Reservation.find(params[:id])
    end

    def new
        @reservation = Reservation.new
        @start_date = params[:start_date]
        @end_date = params[:end_date]
        @price = params[:price].to_i
        session[:villa_id] = params[:villa_id]
        session[:price] = params[:price]
        
        if isDiscountable(@start_date, @end_date)
            @price *= 0.90 
        end
        session[:price] = @price
    end
        
    
    def create
        @reservation = Reservation.new(reservation_params)
        @reservation.user_id = current_user.id
        @reservation.villa_id = session[:villa_id]
        @reservation.price = session[:price]
        @villa = Villa.find(session[:villa_id])
        @reservation.save
         
        if @reservation.save
            @user = current_user.email
            ReservationMailer.reservation(@reservation, @user, @villa).deliver
            flash[:success] = "Réservation effectuée avec succès, vous recevrez un mail de confirmation dans quelques instants."
            redirect_to reservations_path
        end

    end

    private
    def reservation_params
        params.require(:reservation).permit(:start_date, :end_date, :nbAdults, :nbChilds, :price, :description)
    end

end

