class ReservationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.reservation_mailer.new_reservation.subject
  #
  def reservation(reservation, user, villa)
    @reservation = reservation
    @user = user
    @villa = villa
    @start_date = @reservation.start_date.strftime("%B %d, %Y")
    @end_date = @reservation.end_date.strftime("%B %d, %Y")
    puts @reservation

    mail to: "contact@test.fr"
  end
end
